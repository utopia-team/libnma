# Copyright (C) Red Hat, Inc.
# This file is distributed under the same license as the libnma package.
# Tomislav Vujec <tvujec@redhat.com>, 2005.
#
msgid ""
msgstr ""
"Project-Id-Version: libnma\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/libnma/\n"
"POT-Creation-Date: 2023-01-09 00:38+0100\n"
"PO-Revision-Date: 2019-10-17 17:02+0200\n"
"Last-Translator: gogo <linux.hr@protonmail.com>\n"
"Language-Team: Croatian <hr@li.org>\n"
"Language: hr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Launchpad-Export-Date: 2017-04-09 22:32+0000\n"
"X-Generator: Poedit 2.2.1\n"

#: org.gnome.nm-applet.eap.gschema.xml.in:6
#: org.gnome.nm-applet.eap.gschema.xml.in:11
msgid "Ignore CA certificate"
msgstr "Zanemari CA vjerodajnicu"

#: org.gnome.nm-applet.eap.gschema.xml.in:7
msgid ""
"Set this to true to disable warnings about CA certificates in EAP "
"authentication."
msgstr ""
"Odaberite za onemogućavanje upozorenja o CA vjerodajnicama u EAP ovjeri."

#: org.gnome.nm-applet.eap.gschema.xml.in:12
msgid ""
"Set this to true to disable warnings about CA certificates in phase 2 of EAP "
"authentication."
msgstr ""
"Odaberite za onemogućavanje upozorenja o CA vjerodajnicama u drugoj fazi EAP "
"ovjere."

#: shared/nm-utils/nm-shared-utils.c:793
#, c-format
msgid "object class '%s' has no property named '%s'"
msgstr "klasa objekta '%s' nema svojstvo naziva '%s'"

#: shared/nm-utils/nm-shared-utils.c:800
#, c-format
msgid "property '%s' of object class '%s' is not writable"
msgstr "svojstvo '%s' klase objekta '%s' nije zapisivo"

#: shared/nm-utils/nm-shared-utils.c:807
#, c-format
msgid ""
"construct property \"%s\" for object '%s' can't be set after construction"
msgstr ""
"svojstvo izgradnje \"%s\" za objekt '%s' ne može se postaviti nakon "
"izgradenje"

#: shared/nm-utils/nm-shared-utils.c:815
#, c-format
msgid "'%s::%s' is not a valid property name; '%s' is not a GObject subtype"
msgstr "'%s::%s' nije valjani naziv svojstva; '%s' nije GObject podvrsta"

#: shared/nm-utils/nm-shared-utils.c:824
#, c-format
msgid "unable to set property '%s' of type '%s' from value of type '%s'"
msgstr ""
"neuspješno postavljanje svojstva '%s' vrste '%s' iz vrijednosti vrste '%s'"

#: shared/nm-utils/nm-shared-utils.c:835
#, c-format
msgid ""
"value \"%s\" of type '%s' is invalid or out of range for property '%s' of "
"type '%s'"
msgstr ""
"vrijednost \"%s\" vrste '%s' je neispravna ili je izvan raspona za svojstvo "
"'%s' vrste '%s'"

#: src/nma-bar-code-widget.c:140
msgid "Network"
msgstr "Mreža"

#: src/nma-bar-code-widget.c:157
msgid "Password"
msgstr "Lozinka"

#: src/nma-bar-code-widget.ui:35
msgid "Scan with your phone or <a href=\"nma:print\">Print</a>"
msgstr "Skenirajte sa svojim telefonom ili <a href=\"nma:print\">Ispišite</a>"

#: src/nma-cert-chooser.c:513
msgid "No certificate set"
msgstr "Nema postavljenih vjerodajnica"

#: src/nma-cert-chooser.c:537
msgid "No key set"
msgstr "Nema postavljenih ključeva"

#: src/nma-cert-chooser.c:860
#, c-format
msgid "Choose a %s Certificate"
msgstr "Odaberite %s vjerodajnicu"

#: src/nma-cert-chooser.c:864
#, c-format
msgid "%s _certificate"
msgstr "%s _vjerodajnica"

#: src/nma-cert-chooser.c:868
#, c-format
msgid "%s certificate _password"
msgstr "Lozinka %s _vjerodajnice"

#: src/nma-cert-chooser.c:887
#, c-format
msgid "Choose a key for %s Certificate"
msgstr "Odaberite svoj ključ za %s vjerodajnicu"

#: src/nma-cert-chooser.c:891
#, c-format
msgid "%s private _key"
msgstr "%s privatni _ključ"

#: src/nma-cert-chooser.c:895
#, c-format
msgid "%s key _password"
msgstr "%s lozinka _ključa"

#: src/nma-cert-chooser.c:1125
msgid "Sho_w passwords"
msgstr "Prikaž_i lozinku"

#: src/nma-cert-chooser-button.c:177
#, c-format
msgid "Key in %s"
msgstr "Ključ u %s"

#: src/nma-cert-chooser-button.c:178
#, c-format
msgid "Certificate in %s"
msgstr "Vjerodajnica u %s"

#: src/nma-cert-chooser-button.c:209 src/nma-cert-chooser-button.c:320
msgid "Select"
msgstr "Odaberi"

#: src/nma-cert-chooser-button.c:210 src/nma-cert-chooser-button.c:321
msgid "Cancel"
msgstr "Odustani"

#: src/nma-cert-chooser-button.c:277 src/nma-ws/nma-eap-fast.c:329
msgid "(None)"
msgstr "(nijedan)"

#: src/nma-cert-chooser-button.c:300 src/nma-pkcs11-cert-chooser-dialog.c:189
msgid "(Unknown)"
msgstr "(nepoznato)"

#: src/nma-cert-chooser-button.c:431
msgid "Select from file…"
msgstr "Odaberi iz datoteke…"

#: src/nma-mobile-providers.c:818
msgid "Default"
msgstr "Zadano"

#: src/nma-mobile-providers.c:1007
msgid "My country is not listed"
msgstr "Moja država nije navedena"

#: src/nma-mobile-providers.c:1045
msgid "Kosovo"
msgstr ""

#: src/nma-mobile-wizard.c:140
msgid "GSM"
msgstr "GSM"

#: src/nma-mobile-wizard.c:143
msgid "CDMA"
msgstr "CDMA"

#: src/nma-mobile-wizard.c:252 src/nma-mobile-wizard.c:284
msgid "Unlisted"
msgstr "Nenavedeno"

#: src/nma-mobile-wizard.c:483
msgid "My plan is not listed…"
msgstr "Moj plan nije naveden…"

#: src/nma-mobile-wizard.c:654
msgid "Provider"
msgstr "Pružatelj usluge"

#: src/nma-mobile-wizard.c:1026
msgid "Installed GSM device"
msgstr "Instaliran GSM uređaj"

#: src/nma-mobile-wizard.c:1029
msgid "Installed CDMA device"
msgstr "Instaliran CDMA uređaj"

#: src/nma-mobile-wizard.c:1234
msgid "Any device"
msgstr "Bilo koji uređaj"

#: src/nma-mobile-wizard.ui:49
msgid "New Mobile Broadband Connection"
msgstr "Novo mobilno širokopojasno povezivanje"

#: src/nma-mobile-wizard.ui:66
msgid ""
"This assistant helps you easily set up a mobile broadband connection to a "
"cellular (3G) network."
msgstr ""
"Ovaj pomoćnik vam pomaže jednostavno podesiti mobilno širokopojasno "
"povezivanje na mobilnu (3G) mrežu."

#: src/nma-mobile-wizard.ui:81
msgid "You will need the following information:"
msgstr "Bit će vam potrebne sljedeće informacije:"

#: src/nma-mobile-wizard.ui:96
msgid "Your broadband provider’s name"
msgstr "Naziv vašeg pružatelja usluge"

#: src/nma-mobile-wizard.ui:110
msgid "Your broadband billing plan name"
msgstr "Naziv plana naplate vašeg pružatelja usluge"

#: src/nma-mobile-wizard.ui:124
msgid "(in some cases) Your broadband billing plan APN (Access Point Name)"
msgstr ""
"(u nekim slučajevima) NPT (Naziv Pristupne Točke) vašeg plana naplate "
"mobilnog interneta"

#: src/nma-mobile-wizard.ui:138
msgid "Create a connection for _this mobile broadband device:"
msgstr "Stvori povezivanje za _ovaj uređaj mobilnog širokopojasnog Interneta:"

#: src/nma-mobile-wizard.ui:164
msgid "Set up a Mobile Broadband Connection"
msgstr "Postavite povezivanje mobilnog širokopojasnog Interneta"

#: src/nma-mobile-wizard.ui:182
msgid "Country or region:"
msgstr "Država ili regija:"

#: src/nma-mobile-wizard.ui:220
msgid "Choose your Provider’s Country or Region"
msgstr "Odaberite državu ili regiju vašeg pružatelja usluge"

#: src/nma-mobile-wizard.ui:235
msgid "Select your provider from a _list:"
msgstr "Odaberite svojeg pružatelja usluge s _popisa:"

#: src/nma-mobile-wizard.ui:277
msgid "I can’t find my provider and I wish to set up the connection _manually:"
msgstr ""
"Ne mogu pronaći svojega pružatelja usluge i želim povezivanje postaviti "
"_ručno:"

#: src/nma-mobile-wizard.ui:298
msgid "My provider uses GSM technology (GPRS, EDGE, UMTS, HSPA)"
msgstr "Moj pružatelj usluge koristi GSM tehnologiju (GPRS, EDGE, UMTS, HSPA)"

#: src/nma-mobile-wizard.ui:299
msgid "My provider uses CDMA technology (1xRTT, EVDO)"
msgstr "Moj pružatelj usluge koristi CDMA tehnologiju (1xRTT, EVDO)"

#: src/nma-mobile-wizard.ui:310
msgid "Choose your Provider"
msgstr "Odaberite svojeg pružatelja usluge"

#: src/nma-mobile-wizard.ui:327
msgid "_Select your plan:"
msgstr "_Odaberite svoj plan:"

#: src/nma-mobile-wizard.ui:353
msgid "Selected plan _APN (Access Point Name):"
msgstr "Odabrani plan _NPT (Naziv Pristupne Točke):"

#: src/nma-mobile-wizard.ui:401
msgid ""
"Warning: Selecting an incorrect plan may result in billing issues for your "
"broadband account or may prevent connectivity.\n"
"\n"
"If you are unsure of your plan please ask your provider for your plan’s APN."
msgstr ""
"Upozorenje: Odabir krivog plana može uzrokovati financijske troškove na "
"vašem računu mobilnog interneta ili spriječiti povezivanje.\n"
"\n"
"Ako niste sigurni koji plan trebate koristiti, upitajte svojeg pružatelja "
"mobilnog interneta koji je NPT (Naziv Pristupne Točke) vašeg plana."

#: src/nma-mobile-wizard.ui:422
msgid "Choose your Billing Plan"
msgstr "Odaberite svoj način naplate"

#: src/nma-mobile-wizard.ui:440
msgid ""
"Your mobile broadband connection is configured with the following settings:"
msgstr ""
"Vaše mobilno širokopojasno povezivanje je podešeno sa sljedećim postavkama:"

#: src/nma-mobile-wizard.ui:454
msgid "Your Device:"
msgstr "Vaš uređaj:"

#: src/nma-mobile-wizard.ui:480
msgid "Your Provider:"
msgstr "Vaš pružatelj usluge:"

#: src/nma-mobile-wizard.ui:506
msgid "Your Plan:"
msgstr "Vaš plan:"

#: src/nma-mobile-wizard.ui:561
msgid ""
"A connection will now be made to your mobile broadband provider using the "
"settings you selected. If the connection fails or you cannot access network "
"resources, double-check your settings. To modify your mobile broadband "
"connection settings, choose “Network Connections” from the System → "
"Preferences menu."
msgstr ""
"Koristeći odabrane postavke, sada će se uspostaviti povezivanje prema vašem "
"pružatelju usluge mobilnog Interneta.  Ako povezivanje ne uspije ili ne "
"možete pristupiti mrežnim resursima, ponovno provjerite svoje postavke.  "
"Kako biste uredili postavke svog mobilnog Internet povezivanja, odaberite "
"\"Mrežno povezivanje\" u izborniku Sustav → Postavke."

#: src/nma-mobile-wizard.ui:575
msgid "Confirm Mobile Broadband Settings"
msgstr "Potvrdi mobilne širokopojasne postavke"

#: src/nma-pkcs11-cert-chooser-dialog.c:260
msgid "Error logging in: "
msgstr "Greška prijave: "

#: src/nma-pkcs11-cert-chooser-dialog.c:282
msgid "Error opening a session: "
msgstr "Greška pri otvaranju sesije: "

#: src/nma-pkcs11-cert-chooser-dialog.ui:18
msgid "_Unlock token"
msgstr "_Token otključvanja"

#: src/nma-pkcs11-cert-chooser-dialog.ui:99
msgid "Name"
msgstr "Naziv"

#: src/nma-pkcs11-cert-chooser-dialog.ui:109
msgid "Issued By"
msgstr "Izdao"

#: src/nma-pkcs11-token-login-dialog.c:134
#, c-format
msgid "Enter %s PIN"
msgstr "Upiši %s PIN"

#: src/nma-pkcs11-token-login-dialog.ui:19 src/nma-vpn-password-dialog.ui:28
#: src/nma-wifi-dialog.c:1125 src/nma-ws/nma-eap-fast.ui:27
msgid "_Cancel"
msgstr "_Odustani"

#: src/nma-pkcs11-token-login-dialog.ui:34
msgid "_Login"
msgstr "_Prijava"

#: src/nma-pkcs11-token-login-dialog.ui:81
msgid "_Remember PIN"
msgstr "_Zapamti PIN"

#: src/nma-ui-utils.c:32
msgid "Store the password only for this user"
msgstr "Spremi lozinku samo za ovog korisnika"

#: src/nma-ui-utils.c:33
msgid "Store the password for all users"
msgstr "Spremi lozinku za sve korisnike"

#: src/nma-ui-utils.c:34
msgid "Ask for this password every time"
msgstr "Pitaj za ovu lozinku svaki puta"

#: src/nma-ui-utils.c:35
msgid "The password is not required"
msgstr "Lozinka nije potrebna"

#: src/nma-vpn-password-dialog.ui:43
msgid "_OK"
msgstr "_U redu"

#: src/nma-vpn-password-dialog.ui:76
msgid "Sh_ow passwords"
msgstr "Pr_kaži lozinke"

#: src/nma-vpn-password-dialog.ui:133
msgid "_Tertiary Password:"
msgstr "_Treća lozinka:"

#: src/nma-vpn-password-dialog.ui:147
msgid "_Secondary Password:"
msgstr "_Druga lozinka:"

#: src/nma-vpn-password-dialog.ui:161
msgid "_Password:"
msgstr "_Lozinka:"

#: src/nma-wifi-dialog.c:114
msgid "Click to connect"
msgstr "Klikni za povezivanje"

#: src/nma-wifi-dialog.c:441
msgid "New…"
msgstr "Novo…"

#: src/nma-wifi-dialog.c:937
msgctxt "Wifi/wired security"
msgid "None"
msgstr "Nijedno"

#: src/nma-wifi-dialog.c:953
msgid "WEP 40/128-bit Key (Hex or ASCII)"
msgstr "WEP 40/128-bitni ključ (heksadecimalni ili ASCII)"

#: src/nma-wifi-dialog.c:960
msgid "WEP 128-bit Passphrase"
msgstr "WEP 128-bitna lozinka"

#: src/nma-wifi-dialog.c:975 src/nma-ws/nma-ws-802-1x.c:366
msgid "LEAP"
msgstr "LEAP"

#: src/nma-wifi-dialog.c:986
msgid "Dynamic WEP (802.1x)"
msgstr "Promjenjivi WEP (802.1x)"

#: src/nma-wifi-dialog.c:998
msgid "WPA & WPA2 Personal"
msgstr "WPA & WPA2 osobni"

#: src/nma-wifi-dialog.c:1014
msgid "WPA & WPA2 Enterprise"
msgstr "WPA & WPA2 tvrtke"

#: src/nma-wifi-dialog.c:1025
msgid "WPA3 Personal"
msgstr "WPA3 osobni"

#: src/nma-wifi-dialog.c:1036
msgid "Enhanced Open"
msgstr ""

#: src/nma-wifi-dialog.c:1129
msgid "C_reate"
msgstr "S_tvori"

#: src/nma-wifi-dialog.c:1131
msgid "C_onnect"
msgstr "_Spoji se"

#: src/nma-wifi-dialog.c:1209
#, c-format
msgid ""
"Passwords or encryption keys are required to access the Wi-Fi network “%s”."
msgstr ""
"Lozinka ili ključ šifriranja je potreban za pristup bežičnoj mreži \"%s\"."

#: src/nma-wifi-dialog.c:1211
msgid "Wi-Fi Network Authentication Required"
msgstr "Potrebna je ovjera bežične mreže"

#: src/nma-wifi-dialog.c:1213
msgid "Authentication required by Wi-Fi network"
msgstr "Potrebna je ovjera bežične mreže"

#: src/nma-wifi-dialog.c:1218
msgid "Create New Wi-Fi Network"
msgstr "Stvori novu bežičnu mrežu"

#: src/nma-wifi-dialog.c:1220
msgid "New Wi-Fi network"
msgstr "Nova bežična mreža"

#: src/nma-wifi-dialog.c:1221
msgid "Enter a name for the Wi-Fi network you wish to create."
msgstr "Upišite naziv bežične mreže koju želite stvoriti."

#: src/nma-wifi-dialog.c:1223
msgid "Connect to Hidden Wi-Fi Network"
msgstr "Poveži se na skrivenu bežičnu mrežu"

#: src/nma-wifi-dialog.c:1225
msgid "Hidden Wi-Fi network"
msgstr "Skrivena bežična mreža"

#: src/nma-wifi-dialog.c:1226
msgid ""
"Enter the name and security details of the hidden Wi-Fi network you wish to "
"connect to."
msgstr ""
"Upišite naziv i sigurnosne pojedinosti skrivene mreže na koju se želite "
"povezati."

#: src/nma-ws/nma-eap-fast.c:59
msgid "missing EAP-FAST PAC file"
msgstr "nedostaje EAP-FAST PAC datoteka"

#: src/nma-ws/nma-eap-fast.c:249 src/nma-ws/nma-eap-peap.c:309
#: src/nma-ws/nma-eap-ttls.c:362
msgid "GTC"
msgstr "GTC"

#: src/nma-ws/nma-eap-fast.c:265 src/nma-ws/nma-eap-peap.c:277
#: src/nma-ws/nma-eap-ttls.c:296
msgid "MSCHAPv2"
msgstr "MSCHAPv2"

#: src/nma-ws/nma-eap-fast.c:448
msgid "PAC files (*.pac)"
msgstr "PAC datoteke (*.pac)"

#: src/nma-ws/nma-eap-fast.c:452
msgid "All files"
msgstr "Sve datoteke"

#: src/nma-ws/nma-eap-fast.ui:19
msgid "Choose a PAC file"
msgstr "Odaberite PAC datoteku"

#: src/nma-ws/nma-eap-fast.ui:36
msgid "_Open"
msgstr "_Otvori"

#: src/nma-ws/nma-eap-fast.ui:72
msgid "Anonymous"
msgstr "Anonimno"

#: src/nma-ws/nma-eap-fast.ui:75
msgid "Authenticated"
msgstr "Ovjereno"

#: src/nma-ws/nma-eap-fast.ui:78
msgid "Both"
msgstr "Oboje"

#: src/nma-ws/nma-eap-fast.ui:91 src/nma-ws/nma-eap-peap.ui:42
#: src/nma-ws/nma-eap-ttls.ui:113
msgid "Anony_mous identity"
msgstr "Anoni_mni identitet"

#: src/nma-ws/nma-eap-fast.ui:117
msgid "PAC _file"
msgstr "PAC _datoteka"

#: src/nma-ws/nma-eap-fast.ui:188 src/nma-ws/nma-eap-peap.ui:115
#: src/nma-ws/nma-eap-ttls.ui:71
msgid "_Inner authentication"
msgstr "_Unutarnja ovjera"

#: src/nma-ws/nma-eap-fast.ui:217
msgid "Allow automatic PAC pro_visioning"
msgstr "Dopusti automatsko PAC dodijelji_vanje"

#: src/nma-ws/nma-eap-leap.c:52
msgid "missing EAP-LEAP username"
msgstr "nedostaje EAP-LEAP korisničko ime"

#: src/nma-ws/nma-eap-leap.c:61
msgid "missing EAP-LEAP password"
msgstr "nedostaje EAP-LEAP lozinka"

#: src/nma-ws/nma-eap-leap.ui:15 src/nma-ws/nma-eap-simple.ui:15
#: src/nma-ws/nma-ws-leap.ui:15
msgid "_Username"
msgstr "_Korisničko ime"

#: src/nma-ws/nma-eap-leap.ui:29 src/nma-ws/nma-eap-simple.ui:29
#: src/nma-ws/nma-ws-leap.ui:29 src/nma-ws/nma-ws-sae.ui:14
#: src/nma-ws/nma-ws-wpa-psk.ui:14
msgid "_Password"
msgstr "_Lozinka"

#: src/nma-ws/nma-eap-leap.ui:54 src/nma-ws/nma-eap-simple.ui:71
#: src/nma-ws/nma-ws-leap.ui:55 src/nma-ws/nma-ws-sae.ui:56
#: src/nma-ws/nma-ws-wpa-psk.ui:55
msgid "Sho_w password"
msgstr "Pri_kaži lozinku"

#: src/nma-ws/nma-eap-peap.c:293 src/nma-ws/nma-eap-ttls.c:346
#: src/nma-ws/nma-ws-802-1x.c:342
msgid "MD5"
msgstr "MD5"

#: src/nma-ws/nma-eap-peap.ui:23
msgid "Automatic"
msgstr "Automatski"

#: src/nma-ws/nma-eap-peap.ui:26
msgid "Version 0"
msgstr "Inačica 0"

#: src/nma-ws/nma-eap-peap.ui:29
msgid "Version 1"
msgstr "Inačica 1"

#: src/nma-ws/nma-eap-peap.ui:66 src/nma-ws/nma-eap-tls.ui:38
#: src/nma-ws/nma-eap-ttls.ui:83
msgid "No CA certificate is _required"
msgstr "CA vjerodajnice nisu _potrebne"

#: src/nma-ws/nma-eap-peap.ui:83
msgid "PEAP _version"
msgstr "PEAP _inačica"

#: src/nma-ws/nma-eap-peap.ui:162 src/nma-ws/nma-eap-tls.ui:56
#: src/nma-ws/nma-eap-ttls.ui:127
msgid "Suffix of the server certificate name."
msgstr "Sufiks naziva vjerodajnice poslužitelja."

#: src/nma-ws/nma-eap-peap.ui:163 src/nma-ws/nma-eap-tls.ui:57
#: src/nma-ws/nma-eap-ttls.ui:128
msgid "_Domain"
msgstr "_Domena"

#: src/nma-ws/nma-eap-simple.c:78
msgid "missing EAP username"
msgstr "nedostaje EAP korisničko ime"

#: src/nma-ws/nma-eap-simple.c:94
msgid "missing EAP password"
msgstr "nedostaje EAP lozinka"

#: src/nma-ws/nma-eap-simple.c:108
msgid "missing EAP client Private Key passphrase"
msgstr "nedostaje lozinka privatnog ključa EAP klijenta"

#: src/nma-ws/nma-eap-simple.ui:97
msgid "P_rivate Key Passphrase"
msgstr "L_ozinka privatnog ključa"

#: src/nma-ws/nma-eap-simple.ui:122
msgid "Sh_ow passphrase"
msgstr "P_rikaži lozinku"

#: src/nma-ws/nma-eap-tls.c:44
msgid "missing EAP-TLS identity"
msgstr "nedostaje EAP-TLS identitet"

#: src/nma-ws/nma-eap-tls.c:234
msgid "no user certificate selected"
msgstr "nema odabrane korisničke vjerodajnice"

#: src/nma-ws/nma-eap-tls.c:239
msgid "selected user certificate file does not exist"
msgstr "odabrana datoteka korisničke vjerodajnice ne postoji"

#: src/nma-ws/nma-eap-tls.c:259
msgid "no key selected"
msgstr "nema odabranog ključa"

#: src/nma-ws/nma-eap-tls.c:264
msgid "selected key file does not exist"
msgstr "odabrana datoteka ključa ne postoji"

#: src/nma-ws/nma-eap-tls.ui:14
msgid "I_dentity"
msgstr "I_dentitet"

#: src/nma-ws/nma-eap-ttls.c:264
msgid "PAP"
msgstr "PAP"

#: src/nma-ws/nma-eap-ttls.c:280
msgid "MSCHAP"
msgstr "MSCHAP"

#: src/nma-ws/nma-eap-ttls.c:313
msgid "MSCHAPv2 (no EAP)"
msgstr "MSCHAPv2 (bez EAP)"

#: src/nma-ws/nma-eap-ttls.c:330
msgid "CHAP"
msgstr "CHAP"

#: src/nma-ws/nma-eap.c:34
msgid "undefined error in 802.1X security (wpa-eap)"
msgstr "neodređena greška u 802.1X sigurnosti (wpa-eap)"

#: src/nma-ws/nma-eap.c:342
msgid "no CA certificate selected"
msgstr "nema odabrane CA vjerodajnice"

#: src/nma-ws/nma-eap.c:347
msgid "selected CA certificate file does not exist"
msgstr "odabrana datoteka CA vjerodajnice ne postoji"

#: src/nma-ws/nma-ws-802-1x.c:354
msgid "TLS"
msgstr "TLS"

#: src/nma-ws/nma-ws-802-1x.c:378
msgid "PWD"
msgstr "PWD"

#: src/nma-ws/nma-ws-802-1x.c:389
msgid "FAST"
msgstr "BRZO"

#: src/nma-ws/nma-ws-802-1x.c:400
msgid "Tunneled TLS"
msgstr "Tunelirani TLS"

#: src/nma-ws/nma-ws-802-1x.c:411
msgid "Protected EAP (PEAP)"
msgstr "Zaštićeni EAP (PEAP)"

#: src/nma-ws/nma-ws-802-1x.c:426
msgid "Unknown"
msgstr "Nepoznato"

#: src/nma-ws/nma-ws-802-1x.c:440
msgid "Externally configured"
msgstr "Vanjski podešeno"

#: src/nma-ws/nma-ws-802-1x.ui:25 src/nma-ws/nma-ws-wep-key.ui:95
msgid "Au_thentication"
msgstr "Ov_jera"

#: src/nma-ws/nma-ws-leap.c:70
msgid "missing leap-username"
msgstr "nedostaje leap-korisničko_ime"

#: src/nma-ws/nma-ws-leap.c:86
msgid "missing leap-password"
msgstr "nedostaje leap-lozinka"

#: src/nma-ws/nma-ws-sae.c:72
msgid "missing password"
msgstr "nedostaje lozinka"

#: src/nma-ws/nma-ws-sae.ui:44 src/nma-ws/nma-ws-wpa-psk.ui:43
msgid "_Type"
msgstr "_Vrsta"

#: src/nma-ws/nma-ws-wep-key.c:109
msgid "missing wep-key"
msgstr "nedostaje wep-ključ"

#: src/nma-ws/nma-ws-wep-key.c:116
#, c-format
msgid "invalid wep-key: key with a length of %zu must contain only hex-digits"
msgstr ""
"neispravan wep-ključ: ključ duljine %zu može sadržavati samo heksadecimalne "
"znamenke"

#: src/nma-ws/nma-ws-wep-key.c:124
#, c-format
msgid ""
"invalid wep-key: key with a length of %zu must contain only ascii characters"
msgstr ""
"neispravan wep-ključ: ključ duljine %zu može sadržavati samo ascii znamenke"

#: src/nma-ws/nma-ws-wep-key.c:130
#, c-format
msgid ""
"invalid wep-key: wrong key length %zu. A key must be either of length 5/13 "
"(ascii) or 10/26 (hex)"
msgstr ""
"neispravan wep-ključ: pogrešna duljina ključa %zu. Ključ može sadržavati "
"samo ili 5/13 (ascii) ili 10/26 (heks) duljinu"

#: src/nma-ws/nma-ws-wep-key.c:137
msgid "invalid wep-key: passphrase must be non-empty"
msgstr "neispravan wep-ključ: lozinka ne smije biti prazna"

#: src/nma-ws/nma-ws-wep-key.c:139
msgid "invalid wep-key: passphrase must be shorter than 64 characters"
msgstr "neispravan wep-ključ: lozinka mora biti kraća od 64 znamenke"

#: src/nma-ws/nma-ws-wep-key.ui:12
msgid "Open System"
msgstr "Otvoreni sustav"

#: src/nma-ws/nma-ws-wep-key.ui:15
msgid "Shared Key"
msgstr "Dijeljeni ključ"

#: src/nma-ws/nma-ws-wep-key.ui:26
msgid "1 (Default)"
msgstr "1 (zadano)"

#: src/nma-ws/nma-ws-wep-key.ui:48
msgid "_Key"
msgstr "_Ključ"

#: src/nma-ws/nma-ws-wep-key.ui:77
msgid "Sho_w key"
msgstr "Prika_ži ključ"

#: src/nma-ws/nma-ws-wep-key.ui:128
msgid "WEP inde_x"
msgstr "WEP inde_ks"

#: src/nma-ws/nma-ws-wpa-psk.c:80
#, c-format
msgid ""
"invalid wpa-psk: invalid key-length %zu. Must be [8,63] bytes or 64 hex "
"digits"
msgstr ""
"neispravan wpa-psk: neispravna duljina-ključa %zu. Mora biti [8,63] bajta "
"ili 64 heksadecimalnih znamenki"

#: src/nma-ws/nma-ws-wpa-psk.c:87
msgid "invalid wpa-psk: cannot interpret key with 64 bytes as hex"
msgstr ""
"neispravan wpa-psk: nemoguće protumačiti ključ od 64 bajta kao heksadecimalni"

#: src/nma-ws/nma-ws.c:40
msgid "Unknown error validating 802.1X security"
msgstr "Nepoznata greška provjere 802.1X sigurnosti"

#. The %s is a mobile provider name, eg "T-Mobile"
#: src/utils/utils.c:161
#, c-format
msgid "%s connection"
msgstr "%s povezivanje"

#: src/utils/utils.c:462
msgid "PEM certificates (*.pem, *.crt, *.cer)"
msgstr "PEM vjerodajnice (*.pem, *.crt, *.cer)"

#: src/utils/utils.c:475
msgid "DER, PEM, or PKCS#12 private keys (*.der, *.pem, *.p12, *.key)"
msgstr "DER, PEM ili PKCS#12 privatni ključevi (*.der, *.pem, *.p12, *.key)"

#: src/wifi.ui:97
msgid "Wi-Fi _security"
msgstr "Bežična _sigurnost"

#: src/wifi.ui:129
msgid "_Network name"
msgstr "_Naziv mreže"

#: src/wifi.ui:154
msgid "C_onnection"
msgstr "P_ovezivanje"

#: src/wifi.ui:179
msgid "Wi-Fi _adapter"
msgstr "Bežični _uređaj"
